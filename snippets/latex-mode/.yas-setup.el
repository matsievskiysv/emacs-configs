(require 'yasnippet)

(defun yas-latex-env-size-interaction ()
  "Choose the size of float environment."
  (yas-choose-value
   '("width=\\columnwidth"
     "height=\\textheight"
     "scale=1")))
