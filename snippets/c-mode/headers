# -*- mode: snippet; require-final-newline: nil -*-
# name: headers
# key: headers
# --
#include <assert.h> 		/* Conditionally compiled macro that compares its argument to zero */
#include <complex.h> 		/* Complex number arithmetic (since C99) */
#include <ctype.h> 		/* Functions to determine the type contained in character data */
#include <errno.h> 		/* Macros reporting error conditions */
#include <fenv.h> 		/* Floating-point environment (since C99) */
#include <float.h> 		/* Limits of float types */
#include <inttypes.h> 		/* Format conversion of integer types (since C99) */
#include <iso646.h> 		/* Alternative operator spellings (since C95 )*/
#include <limits.h> 		/* Sizes of basic types */
#include <locale.h> 		/* Localization utilities */
#include <math.h> 		/* Common mathematics functions */
#include <setjmp.h> 		/* Nonlocal jumps */
#include <signal.h> 		/* Signal handling */
#include <stdalign.h> 		/* alignas and alignof convenience macros (since C11) */
#include <stdarg.h> 		/* Variable arguments */
#include <stdatomic.h> 	/* Atomic types (since C11) */
#include <stdbool.h> 		/* Boolean type (since C99) */
#include <stddef.h> 		/* Common macro definitions */
#include <stdint.h> 		/* Fixed-width integer types (since C99) */
#include <stdio.h> 		/* Input/output */
#include <stdlib.h> 		/* General utilities: memory management, program utilities, string conversions, random numbers */
#include <stdnoreturn.h> 	/* noreturn convenience macros (since C11) */
#include <string.h> 		/* String handling */
#include <tgmath.h> 		/* Type-generic math (macros wrapping math.h and complex.h) (since C99) */
#include <threads.h> 		/* Thread library (since C11) */
#include <time.h> 		/* Time/date utilities */
#include <uchar.h> 		/* UTF-16 and UTF-32 character utilities (since C11) */
#include <wchar.h> 		/* Extended multibyte and wide character utilities (since C95) */
#include <wctype.h> 		/* Functions to determine the type contained in wide character data (since C95) */