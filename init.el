;;; init.el --- Initialize Emacs -*- lexical-binding: t -*-

;; Homepage: https://gitlab.com/matsievskiysv/emacs-configs


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize Emacs

;;; Code:

;; {{{ CheckVer
(when (version< emacs-version "26.1")
  (error "Configuration requires Emacs 26.1 and above!"))

;; Run early-init.el manually
(when (version< emacs-version "27")
  (load (expand-file-name "early-init.el" user-emacs-directory)))
;; }}}

;; {{{ BetterGC
(defvar better-gc-cons-threshold (* 128 1024 1024)
  "The default value to use for `gc-cons-threshold'.

If you experience freezing, decrease this.  If you experience stuttering, increase this.")

(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold better-gc-cons-threshold
                  file-name-handler-alist file-name-handler-alist-original)
            (makunbound 'file-name-handler-alist-original)))
;; }}}

;; {{{ AutoGC
(add-hook 'emacs-startup-hook
          (lambda ()
            (if (boundp 'after-focus-change-function)
                (add-function :after after-focus-change-function
                              (lambda ()
                                (unless (frame-focus-state)
                                  (garbage-collect))))
              (add-hook 'after-focus-change-function 'garbage-collect))
            (defun gc-minibuffer-setup-hook ()
              (setq gc-cons-threshold (* better-gc-cons-threshold 2)))

            (defun gc-minibuffer-exit-hook ()
              (garbage-collect)
              (setq gc-cons-threshold better-gc-cons-threshold))

            (add-hook 'minibuffer-setup-hook #'gc-minibuffer-setup-hook)
            (add-hook 'minibuffer-exit-hook #'gc-minibuffer-exit-hook)))
;; }}}

;; {{{ LoadPath
(defun update-to-load-path (folder)
  "Update FOLDER and its subdirectories to `load-path'."
  (let ((base folder))
    (unless (member base load-path)
      (add-to-list 'load-path base))
    (dolist (f (directory-files base))
      (let ((name (concat base "/" f)))
        (when (and (file-directory-p name)
                   (not (equal f ".."))
                   (not (equal f ".")))
          (unless (member base load-path)
            (add-to-list 'load-path name)))))))

(update-to-load-path (expand-file-name "elisp" user-emacs-directory))
;; }}}

;; {{{ Daemon
(require 'init-server)
;; }}}

;; {{{ Package Management
(require 'init-package)
;; }}}

;; {{{ Path
(require 'init-path)
;; }}}

;; {{{ Constants
(require 'init-const)
;; }}}

;; {{{ Libraries
(require 'init-libraries)
;; }}}

;; {{{ Global Functionalities
(require 'init-global-config)
(require 'init-which-key)
(require 'init-autosave)
(require 'init-func)
(require 'init-multistate)
(require 'init-undo-tree)
;; }}}

;; {{{ Key bindings
(require 'init-keybindings)
;; }}}

;; {{{ Common packages
(require 'init-electricity)
(require 'init-ibuffer)
(require 'init-search)
(require 'init-indent-or-complete)
(require 'init-avy)
(require 'init-visual-kill-ring)
(require 'init-discover-my-major)
(require 'init-shell)
(require 'init-dired)
(require 'init-flyspell)
(require 'init-visual-replace)
(require 'init-expand-region)
(require 'init-code-folding)
(require 'init-eldoc)
(require 'init-comint)
(require 'init-math-preview)
;; }}}

;; {{{ User Interface Enhancements
(require 'init-windows)
(require 'init-display-buffer-control)
(require 'init-ui-config)
(require 'init-icons)
(require 'init-theme)
(require 'init-fonts)
(require 'init-scroll)
(require 'init-mode-line)
;; }}}

;; {{{ General Programming
(require 'init-editor-config)
(require 'init-imenu)
(require 'init-project)
(require 'init-yasnippet)
(require 'init-flycheck)
(require 'init-parens)
(require 'init-indent)
(require 'init-quickrun)
(require 'init-ein)
(require 'init-company)
(require 'init-magit)
(require 'init-drag-stuff)
(require 'init-lsp)
(require 'init-docker)
;; }}}

;; {{{ Programming
(require 'init-elisp)
(require 'init-asm)
(require 'init-cc)
(require 'init-doxygen)
(require 'init-gnuplot)
(require 'init-python)
(require 'init-haskell)
(require 'init-lua)
(require 'init-java)
(require 'init-kotlin)
(require 'init-rust)
(require 'init-spice)
(require 'init-ess)
(require 'init-meson)
(require 'init-octave)
(require 'init-vhdl)
;; }}}

;; {{{ Text editing
(require 'init-csv)
(require 'init-kconfig)
(require 'init-adoc)
(require 'init-markdown)
(require 'init-latex)
(require 'init-insert-math-symbol)
;; }}}

;; {{{ Web Development
(require 'init-webdev)
(require 'init-nginx)
;; }}}

;; {{{ Miscellaneous
(require 'init-gcode)
(require 'init-dts)
(require 'init-gmsh)
(require 'init-mermaid)
(require 'init-org)
(require 'init-system)
(require 'init-eww)
(require 'init-tramp)
;; }}}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; init.el ends here
