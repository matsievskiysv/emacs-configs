;; This file is automatically generated by the multiple-cursors extension.
;; It keeps track of your preferences for running commands with multiple cursors.

(setq mc/cmds-to-run-for-all
      '(
	LaTeX-babel-insert-hyphen
	LaTeX-insert-item
	LaTeX-insert-left-brace
	TeX-insert-backslash
	TeX-insert-quote
	asm-colon
	beginning-of-visual-line
	c-electric-brace
	c-electric-colon
	c-electric-lt-gt
	c-electric-paren
	c-electric-pound
	c-electric-semi&comma
	c-electric-slash
	c-electric-star
	capitalize-dwim
	comment-line
	downcase-dwim
	duplicate-current-lines
	end-of-visual-line
	ess-smart-comma
	ess-yank
	hungry-delete-backward
	hungry-delete-forward
	kill-current-lines
	kill-region
	markdown-end-of-line
	markdown-outdent-or-delete
	py-electric-colon
	py-electric-delete
	sp-backward-sexp
	sp-forward-sexp
	tab-indent-or-complete
	undefined
	upcase-dwim
	vhdl-electric-comma
	vhdl-electric-space
	web-mode-element-insert
	yaml-electric-backspace
	))

(setq mc/cmds-to-run-once
      '(
	avy-goto-word-0
	beginning-of-buffer
	browse-kill-ring
	counsel-M-x
	counsel-find-file
	dap-tooltip-mouse-motion
	describe-key
	end-of-buffer
	goto-line
	handle-switch-frame
	make-frame-command
	math-preview-clear-at-point
	multistate-edit-state
	multistate-emacs-state
	multistate-move-state
	multistate-terminal-state
	multistate-window-state
	overwrite-mode
	scroll-down
	shell-command
	sp-remove-active-pair-overlay
	swiper-isearch
	toggle-input-method
	toggle-truncate-lines
	))
