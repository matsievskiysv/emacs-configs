;;; init-ein.el --- Initialize ein -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize ein

;;; Code:

(use-package ein-notebook
  :ensure ein
  :init
  (use-package polymode :defer t)
  (use-package edit-indirect :defer t)
  :custom
  (ein:use-auto-complete nil)
  (ein:output-area-inlined-images t)
  (ein:truncate-long-cell-output 20)
  (ein:cell-max-num-outputs 20)
  (ein:enable-eldoc-support t)
  (ein:notebook-kill-buffer-ask t)
  (ein:cell-traceback-level 10)
  :bind
  (:map ein:notebook-mode-map
   ("C-c C-x C-e" . ein:worksheet-execute-all-cells)
   ("C-c C-x C-a" . ein:worksheet-execute-all-cells-above)
   ("C-c C-x C-b" . ein:worksheet-execute-all-cells-below)))

(provide 'init-ein)

;;; init-ein.el ends here
