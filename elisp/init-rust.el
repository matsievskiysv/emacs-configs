;;; init-rust.el --- Initialize rust-mode -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize rust-mode

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package rustic
  :if *cargo*
  :mode ("\\.rs\\'" . rustic-mode)
  :custom
  (lsp-rust-all-features t)
  (lsp-rust-analyzer-cargo-all-targets t)
  (lsp-rust-analyzer-cargo-load-out-dirs-from-check t)
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-parameter-hints t)
  (lsp-rust-analyzer-proc-macro-enable t)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-build-on-save t)
  (lsp-rust-full-docs t)
  (lsp-rust-unstable-features nil)
  (rustic-lsp-server 'rust-analyzer)
  (lsp-rust-server 'rust-analyzer)
  :hook
  ((rustic-mode . flycheck-mode)
   (rustic-mode . flycheck-rust-setup))
  :init
  (when *rust-lsp*
    (add-hook 'rustic-mode-hook #'lsp-maybe)))

(use-package flycheck-rust
  :defer t)
(use-package flymake-rust
  :disabled)

(provide 'init-rust)

;;; init-rust.el ends here
