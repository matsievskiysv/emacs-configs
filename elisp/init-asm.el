;;; init-asm.el --- Initialize asm mode -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize asm mode

;;; Code:

(use-package asm-mode
  :init
  (setq asm-comment-char ?@)
  :hook
  (asm-mode . (lambda () (setq comment-start "@ "
			  fill-column 40))))

(provide 'init-asm)

;;; init-asm.el ends here
