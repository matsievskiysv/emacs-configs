;;; init-company.el --- Initialize company -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize company

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package company
  :diminish company-mode
  :ensure
  :custom
  ;; (company-minimum-prefix-length 1)
  ;; (company-tooltip-align-annotations t)
  ;; (company-begin-commands '(self-insert-command))
  ;; (company-require-match 'never)
  ;; Trigger completion immediately.
  (company-idle-delay 1)
  ;; Number the candidates (use M-1, M-2 etc to select completions).
  (company-show-numbers t)
  :bind
  ("C-x <C-tab>" . company-complete)
  :config
  (global-company-mode 1))

(use-package company-box
  :diminish
  :hook (company-mode . company-box-mode))

(provide 'init-company)

;;; init-company.el ends here
