;;; init-magit.el --- Initialize magit -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize magit

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package magit
  :if *git*
  :bind
  ("C-x g" . magit-status)
  :init
  (add-hook 'git-commit-setup-hook 'git-commit-turn-on-flyspell))

(use-package magit-todos
  :custom
  (magit-todos-exclude-globs '(".git" ".svn"))
  (magit-todos-submodule-list nil))

(with-eval-after-load 'ediff
  (custom-set-variables '(ediff-merge-split-window-function 'split-window-vertically))
  (add-hook 'ediff-keymap-setup-hook
            (lambda ()
              (bind-key "<down>" (lambda () (interactive) (ediff-scroll-vertically -1)) ediff-mode-map)
              (bind-key "<up>" (lambda () (interactive) (ediff-scroll-vertically 1)) ediff-mode-map)
              ;; (bind-key "<left>" (lambda () (interactive) (ediff-scroll-horizontally -1)) ediff-mode-map)
              ;; (bind-key "<right>" (lambda () (interactive) (ediff-scroll-horizontally 1)) ediff-mode-map)
              (bind-key "<C-up>" #'ediff-previous-difference ediff-mode-map)
              (bind-key "<C-down>" #'ediff-next-difference ediff-mode-map))))

(provide 'init-magit)

;;; init-magit.el ends here
