;;; init-shell.el --- Initialize shell -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize shell

;;; Code:

(eval-when-compile
  (require 'init-const)
  (require 's)
  (require 'f))

(use-package term-keys
  :config (term-keys-mode t)
  (defun term-keys/st-dump-headers (dir)
    "Dump st term headers to directory `DIR'"
    (interactive "D")
    (require 'term-keys-st)
    (with-temp-buffer
      (insert (term-keys/st-config-key))
      (write-region (point-min) (point-max) (f-join dir "config-term-keys-key.h")))
    (with-temp-buffer
      (insert (term-keys/st-config-mappedkeys))
      (write-region (point-min) (point-max) (f-join dir "config-term-keys-mappedkeys.h"))))
  (add-hook 'server-after-make-frame-hook
            (lambda () (if (not (display-graphic-p))
                      (term-keys/init)))))

;; https://stackoverflow.com/a/7053298/5745120
(defun sh-send-line-or-region-2 (&optional step)
  "Send line or region to shell and optionally `STEP'."
  (interactive)
  (let ((proc (get-process "shell"))
        (win (selected-window))
        (buf (buffer-name))
        pbuf min max command)
    (unless proc
      (let ((currbuff (current-buffer)))
        (shell)
        (switch-to-buffer currbuff)
        (setq proc (get-process "shell"))))
    (setq pbuff (process-buffer proc))
    (if (use-region-p)
        (setq min (region-beginning)
              max (region-end))
      (setq min (point-at-bol)
            max (point-at-eol)))
    (setq command (s-replace "\n\n" "\n" (concat (buffer-substring-no-properties min max) "\n")))
    (process-send-string proc command)
    (display-buffer (process-buffer proc))
    (select-window win)
    (when step
      (goto-char max)
      (next-line))))

(defun sh-send-line-or-region-and-step-2 ()
  "Send line or region to shell."
  (interactive)
  (sh-send-line-or-region-2 t))

(defun sh-switch-to-process-buffer ()
  "Switch to shell process."
  (interactive)
  (pop-to-buffer (process-buffer (get-process "shell")) t))

(use-package sh
  :ensure nil
  :mode (("\\.sh\\'" . sh-mode)
         ("\\.zsh\\'" . sh-mode))
  :init
  (when *shell-lsp*
    (add-hook 'sh-mode-hook #'lsp-maybe))
  :hook
  ((sh-mode . flymake-mode)
   (sh-mode . (lambda () (bind-key "<C-return>" #'sh-send-line-or-region-and-step-2 sh-mode-map)))))

(use-package flymake-shell)

(provide 'init-shell)

;;; init-shell.el ends here
