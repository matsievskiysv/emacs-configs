;;; init-multistate.el --- Initialize multistate -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize multistate

;;; Code:

(defun go-back-third-of-screen ()
  (interactive)
  (previous-line (/ (count-lines (window-start) (window-end)) 3)))

(defun go-forward-third-of-screen ()
  (interactive)
  (next-line (/ (count-lines (window-start) (window-end)) 3)))

(defun multistate-show-keybindings ()
  (interactive)
  (which-key-show-keymap (multistate-manage-variables 'keymap) t))

(use-package multistate
  :custom
  (multistate-suppress-no-digits nil)
  (multistate-deactivate-input-method-on-switch t)
  :hook
  (multistate-emacs-state-enter . (lambda () (hl-line-mode 0)))
  (multistate-emacs-state-exit . (lambda () (hl-line-mode 1)))
  (multistate-terminal-state-enter . (lambda () (hl-line-mode 0)))
  (multistate-terminal-state-exit . (lambda () (hl-line-mode 1)))
  (multistate-window-state-enter . (lambda () (let ((window (selected-window)))
                                           (mapcar (lambda (w)
                                                     (with-selected-window w
                                                       (unless (eq window w)
                                                         (multistate-window-state t t))))
                                                   (window-list)))))
  (multistate-window-state-exit . (lambda () (let ((window (selected-window)))
                                          (mapcar (lambda (w)
                                                    (with-selected-window w
                                                      (unless (eq window w)
                                                        (multistate-emacs-state t t))))
                                                  (window-list)))))
  ;; (multistate-change-state . (lambda () (unless (eq (multistate-manage-variables 'name) 'emacs)
  ;;                                    (multistate-show-keybindings))))
  (help-mode . (lambda () (when (eq major-mode 'help-mode) (multistate-move-state))))
  :init
  (multistate-define-state 'emacs :default t)
  (multistate-define-state
   'move
   :lighter "move"
   :cursor 'hollow
   :parent 'multistate-suppress-map)
  (multistate-define-state
   'command
   :lighter "cmd"
   :cursor 'hollow
   :parent 'multistate-suppress-map)
  (multistate-define-state
   'edit
   :lighter "edit"
   :cursor 'hollow
   :parent 'multistate-move-state-map)
  (multistate-define-state
   'window
   :lighter "win"
   :cursor nil
   :parent 'multistate-suppress-map)
  (multistate-define-state
   'terminal
   :lighter "term")
  (multistate-global-mode t)
  (bind-key* "C-z" #'multistate-emacs-state)
  (bind-key* "M-z" #'multistate-emacs-state)
  (bind-key* "M-n" #'multistate-command-state)
  (bind-key* "M-m" #'multistate-move-state)
  (bind-key* "M-r" #'multistate-edit-state)
  (bind-key* "M-q" #'multistate-window-state)
  (bind-key* "M-o" #'multistate-terminal-state)
  :bind
  (:map multistate-emacs-state-map
        ("C-z" . multistate-emacs-state)
        ("M-z" . multistate-emacs-state)
        ("M-n" . multistate-command-state)
        ("M-m" . multistate-move-state)
        ("M-r" . multistate-edit-state)
        ("M-q" . multistate-window-state))
  (:map multistate-move-state-map
        ("RET" . multistate-emacs-state)
        ("?" . multistate-show-keybindings)
        ("a" . move-beginning-of-line)
        ("A" . back-to-indentation)
        ("e" . move-end-of-line)
        (";" . forward-char)
        ("j" . backward-char)
        ("k" . previous-line)
        ("l" . next-line)
        ("J" . backward-word)
        (":" . forward-word)
        ("K" . go-back-third-of-screen)
        ("L" . go-forward-third-of-screen)
        ("C-k" . backward-paragraph)
        ("C-l" . forward-paragraph)
        ("p" . scroll-down)
        ("n" . scroll-up)
        ("P" . beginning-of-buffer)
        ("N" . end-of-buffer)
        ("y" . yank)
        ("Y" . popup-kill-ring)
        ("w" . kill-ring-save)
        ("W" . kill-region)
        ("i" . imenu)
        ("o" . imenu-list)
        ("d RET" . smerge-keep-current)
        ("d C" . smerge-combine-with-next)
        ("d E" . smerge-ediff)
        ("d R" . smerge-refine)
        ("d a" . smerge-keep-all)
        ("d b" . smerge-keep-base)
        ("d l" . smerge-keep-lower)
        ("d u" . smerge-keep-upper)
        ("d n" . smerge-next)
        ("d p" . smerge-prev)
        ("d r" . smerge-resolve)
        ("d = <" . smerge-diff-base-upper)
        ("d = =" . smerge-diff-upper-lower)
        ("d = >" . smerge-diff-base-lower))
  (:map multistate-command-state-map
        ("RET" . multistate-emacs-state)
        ("?" . multistate-show-keybindings)
        ("b l" . bookmark-bmenu-list)
        ("b SPC" . bookmark-jump)
        ("b W" . bookmark-write)
        ("b L" . bookmark-load)
        ("b n" . bookmark-set)
        ("x w" . delete-trailing-whitespace)
        ("x o" . menu-bar-mode))
  (:map multistate-edit-state-map
        ("RET" . multistate-emacs-state)
        ("?" . multistate-show-keybindings)
        ("d" . kill-current-lines)
        ("D" . duplicate-current-lines)
        ("f" . kill-word)
        ("F" . kill-sentence)
        ("b" . backward-kill-word)
        ("B" . backward-kill-sentence)
        ("c l" . downcase-dwim)
        ("c u" . upcase-dwim)
        ("c c" . capitalize-dwim)
        ("i f" . fill-paragraph)
        ("i s" . split-sentences))
  (:map multistate-window-state-map
        ("SPC" . multistate-emacs-state)
        ("RET" . multistate-emacs-state)
        ("?" . multistate-show-keybindings)
        ("h" . split-window-horizontally)
        ("v" . split-window-vertically)
        ("j" . windmove-left)
        ("k" . windmove-up)
        ("l" . windmove-down)
        (";" . windmove-right)
        ("<left>" . windmove-left)
        ("<up>" . windmove-up)
        ("<down>" . windmove-down)
        ("<right>" . windmove-right)
        ("J" . shrink-window)
        (":" . enlarge-window)
        ("K" . shrink-window-horizontally)
        ("L" . enlarge-window-horizontally)
        ("1" . delete-other-windows)
        ("0" . delete-window)
        ("<delete>" . kill-this-buffer))
  (:map multistate-terminal-state-map
        ("TAB" . tab-indent-or-complete)
        ("C-x TAB" . company-complete)
        ("C-x ;" . comment-line)
        ("`" . mode-line-other-buffer)
        ))

(provide 'init-multistate)

;;; init-multistate.el ends here
