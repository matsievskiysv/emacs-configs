;;; init-indent.el --- Initialize indentation -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize indentation

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package highlight-indent-guides
  :if *sys/gui*
  :diminish
  :hook ((prog-mode text-mode web-mode nxml-mode) . highlight-indent-guides-mode)
  :custom
  (highlight-indent-guides-method 'fill)
  (highlight-indent-guides-responsive nil)
  (highlight-indent-guides-delay 0.1)
  (highlight-indent-guides-auto-character-face-perc 7))

(custom-set-variables '(indent-tabs-mode t))
(custom-set-variables '(tab-width 8))
;; (setq-default indent-line-function 'insert-tab)
(add-hook 'after-change-major-mode-hook
          (lambda () (if (equal electric-indent-mode 't)
                    (when (derived-mode-p 'prog-mode)
                      (electric-indent-mode 1))
                  (electric-indent-mode -1))))

(defun toggle-indent-tab-mode ()
  "Toggle use tabs."
  (interactive)
  (if indent-tabs-mode
      (progn
	(message "Indent using spaces")
	(setq indent-tabs-mode nil))
    (progn
      (message "Indent using tabs")
      (setq indent-tabs-mode t))))

(defun set-tab-width (w)
  "Set tab width.
W - width of tab"
  (interactive "nEnter tab width: ")
  (setq tab-width w))

;; https://emacs.stackexchange.com/a/32999/21814
(defun how-many-region (begin end regexp &optional interactive)
  "Print number of non-trivial matches for REGEXP in region.
   Non-interactive arguments are Begin End Regexp"
  (interactive "r\nsHow many matches for (regexp): \np")
  (let ((count 0) opoint)
    (save-excursion
      (setq end (or end (point-max)))
      (goto-char (or begin (point)))
      (while (and (< (setq opoint (point)) end)
                  (re-search-forward regexp end t))
        (if (= opoint (point))
            (forward-char 1)
          (setq count (1+ count))))
      (if interactive (message "%d occurrences" count))
      count)))

;; https://emacs.stackexchange.com/a/32999/21814
(defun infer-indentation-style ()
  ;; if our source file uses tabs, we use tabs, if spaces spaces, and if
  ;; neither, we use the current indent-tabs-mode
  (let ((space-count (how-many-region (point-min) (point-max) "^  "))
        (tab-count (how-many-region (point-min) (point-max) "^\t")))
    (if (> space-count tab-count) (setq indent-tabs-mode nil))
    (if (> tab-count space-count) (setq indent-tabs-mode t))))

(add-hook 'prog-mode-hook 'infer-indentation-style)

(provide 'init-indent)

;;; init-indent.el ends here
