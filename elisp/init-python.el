;;; init-python.el --- -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialize python

;;; Code:

(eval-when-compile
  (require 'init-flycheck)
  (require 'init-const))

(use-package elpy
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :custom
  (elpy-rpc-python-command "python3")
  (python-shell-interpreter "ipython3")
  (python-shell-interpreter-args "-i --simple-prompt")
  (lsp-pylsp-plugins-flake8-max-line-length 80)
  :hook
  (python-mode . (lambda ()
                   (unless (string-match "\\*ein:" (buffer-name))
                     (elpy-mode))))
  :bind
  (:map python-mode-map
        ("C-x x b" . elpy-shell-send-buffer)
	("<C-return>" . (lambda () (interactive)
			  (if (use-region-p)
			      (progn
				(elpy-shell-send-region-or-buffer-and-step)
				(setq mark-active nil)
				(python-nav-forward-statement))
			    (elpy-shell-send-statement-and-step)))))
  :init
  (when *pyls*
    (add-hook 'elpy-mode-hook #'lsp-maybe))
  :config
  (elpy-enable)
  (unbind-key "<C-return>" elpy-mode-map))

(use-package python-mode
  :disabled
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :custom
  (python-shell-interpreter "ipython3")
  (python-shell-interpreter-args "-i --simple-prompt")
  (lsp-pylsp-plugins-flake8-max-line-length 80)
  :bind
  (:map python-mode-map
        ("C-x x b" . elpy-shell-send-buffer)
	("<C-return>" . (lambda () (interactive)
			  (if (use-region-p)
			      (progn
				(py-execute-region (region-beginning) (region-end))
				(setq mark-active nil))
			    (py-execute-line)))))
  :init
  (when *pyls*
    (add-hook 'python-mode-hook #'lsp-maybe)))

(use-package jedi
  :disabled)

(provide 'init-python)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; init-python.el ends here
