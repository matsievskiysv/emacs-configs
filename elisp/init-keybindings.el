;;; init-keybindings.el --- General keybindings -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; General keybindings

;;; Code:

;; Unbind unneeded keys
(unbind-key "<mouse-2>")                ; mouse
(unbind-key "C-v")                      ; Scroll up
(unbind-key "M-v")                      ; Scroll down

;; {{{ General emacs keybindings - persistent
;; Adjust font size like web browsers
(bind-key "C-+" #'text-scale-increase)
(bind-key "C--" #'text-scale-decrease)
;; Truncate lines
(bind-key "C-x C-l" #'toggle-truncate-lines)
;; Comment regions
(bind-key "C-;" #'comment-dwim)
;; Comment set column
(bind-key "C-x ;" #'comment-set-column-arg)
;; Case edit commands
(bind-key "M-l" #'downcase-dwim)
(bind-key "M-c" #'capitalize-dwim)
(bind-key "M-u" #'upcase-dwim)
;; Switch to other buffer
(bind-key "C-`" #'mode-line-other-buffer)
(bind-key "C-x C-a" #'back-to-indentation)
 ;; Edit line at point
(bind-key "C-k" #'kill-current-lines)
(bind-key "M-k" #'duplicate-current-lines)
;; }}}

(custom-set-variables
 '(gud-key-prefix (kbd "C-x x a")))

(provide 'init-keybindings)

;;; init-keybindings.el ends here
