;;; init-fonts.el --- Initialize fonts -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize fonts

;;; Code:

(eval-when-compile
  (require 'init-const)
  (require 'fira-code-mode)
  (require 'f))


;; FontsList
(defvar font-list '(("Fira Code" . 18)
		    ("Liberation Mono" . 18)
		    ("DejaVu Sans Mono" . 18)
		    ("FreeMono" . 18)
		    ("Input" . 18))
  "List of fonts and sizes.  The first one available will be used.")

;; FontFun
(defun change-font ()
  "Change font."
  (interactive)
  (let (available-fonts font-name font-size font-setting)
    (dolist (font font-list (setq available-fonts (nreverse available-fonts)))
      (when (member (car font) (font-family-list))
        (push font available-fonts)))
    (if (not available-fonts)
        (message "No fonts from the chosen set are available")
      (if (called-interactively-p 'interactive)
          (let* ((chosen (assoc-string (completing-read "What font to use? " available-fonts nil t) available-fonts)))
            (setq font-name (car chosen) font-size (read-number "Font size: " (cdr chosen))))
        (setq font-name (caar available-fonts) font-size (cdar available-fonts)))
      (setq font-setting (format "%s-%d" font-name font-size))
      ;; (set-face-attribute 'default nil :font font-setting)
      ;; (set-face-attribute 'fixed-pitch nil :font font-setting)
      (set-frame-font font-setting nil t)
      (add-to-list 'default-frame-alist (cons 'font font-setting)))))

(defun test-install-font-freedesktop (fontname fontfile)
  "Install font fontname from fontfile if it is not avaliable."
  (when (and (not (member fontname (font-family-list)))
	     (f-file? (f-expand fontfile user-emacs-directory)))
    (let ((fontdir (f-expand
		    "share/fonts"
		    (or (getenv "XDG_DATA_HOME")
			(f-expand ".local"
                                  (getenv "HOME")))))
          (name (f-filename fontfile)))
    (when (not (f-exists? (f-expand name (f-full fontdir))))
      (make-directory fontdir t)
      (message "Installing font %s" name)
      (message nil)
      (f-copy  (f-expand fontfile user-emacs-directory) (f-full fontdir))))))

(defun init-fonts-initialize ()
  "Initialize fonts."
  ;; Install fonts
  (test-install-font-freedesktop "FiraCode Regular" "assets/fonts/FiraCode-Regular.ttf")
  (test-install-font-freedesktop "FiraCode Symbol" "assets/fonts/FiraCode-Regular-Symbol.otf")
  ;; select font
  (change-font)
  ;; Use FiraCode for prog-mode
  (when (member "Fira Code Symbol" (font-family-list))
    (add-hook 'prog-mode-hook
              (lambda () (when (display-graphic-p)
                       (fira-code-mode 1))))))

(diminish 'fira-code-mode)

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (with-selected-frame frame
                  (init-fonts-initialize))))
    (init-fonts-initialize))

(provide 'init-fonts)

;;; init-fonts.el ends here
