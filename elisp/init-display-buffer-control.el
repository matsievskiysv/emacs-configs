;;; init-display-buffer-control.el --- Initialize display-buffer-control -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize display-buffer-control

;;; Code:

(use-package dbc
  :custom
  (dbc-verbose nil)
  :config
  (setq dbc-bottom-side-action-repl '((display-buffer-in-side-window) . ((side . bottom) (window-height . 0.4))))
  (setq dbc-bottom-side-small-action-info '((display-buffer-in-side-window) . ((side . bottom) (window-height . 0.2))))
  (dbc-add-ruleset "pop-up-frame" dbc-pop-up-frame-action 100)
  (dbc-add-ruleset "right" dbc-right-action)
  (dbc-add-ruleset "above" dbc-above-action)
  (dbc-add-ruleset "bottom-side" dbc-bottom-side-small-action-info 300)
  (dbc-add-ruleset "big-bottom-side" dbc-bottom-side-action-repl 300)
  (dbc-add-ruleset "same" '(display-buffer-same-window))
  (dbc-add-rule "pop-up-frame" "eww" :newmajor "eww-mode")
  (dbc-add-rule "right" "help" :newname "\\*help\\*")
  (dbc-add-rule "bottom-side" "warnings" :newname "\\*Warnings\\*")
  (dbc-add-rule "same" "from help" :oldname "\\*help\\*")
  (dbc-add-rule "bottom-side" "shell" :oldmajor "sh-mode" :newname "\\*shell\\*")
  (dbc-add-rule "bottom-side" "python repl" :newmajor "inferior-python-mode")
  (dbc-add-rule "bottom-side" "ess repl" :newmajor "inferior-ess-.+-mode")
  (dbc-add-rule "bottom-side" "lua repl" :newmajor "comint-mode" :oldmajor "lua-mode" :newname "\\*lua\\*")
  (dbc-add-rule "bottom-side" "node-js repl" :newmajor "comint-mode" :oldmajor "js2-mode" :newname "\\*Javascript REPL\\*")
  (dbc-add-rule "big-bottom-side" "rustic compile" :newname "\\*rustic-compilation\\*")
  (dbc-add-rule "big-bottom-side" "rustic test" :newname "\\*cargo-test\\*")
  (dbc-add-rule "bottom-side" "octave repl" :newmajor "inferior-octave-mode" :oldmajor "octave-mode"))

(provide 'init-display-buffer-control)

;;; init-display-buffer-control.el ends here
