;;; init-markdown.el --- Initialize markdown-mode -*- lexical-binding: t -*-

;; Author: Matsievskiy S.V.
;; Maintainer: Matsievskiy S.V.
;; Version: 1.0
;; Package-Requires: (markdown-mode)
;; Homepage:
;; Keywords:


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize markdown-mode

;;; Code:

(defun markdown-export-with-name ()
  "Export markdown."
  (interactive)
  (unless (and (bound-and-true-p markdown-export-with-name-file)
               (f-file? markdown-export-with-name-file)
               (f-writable? markdown-export-with-name-file))
    (let ((output (read-file-name "Enter file name: " "/tmp/" nil nil
                                  (concat (f-base (buffer-name)) ".html"))))
      (unless (eq (s-downcase (f-ext output)) "html")
        (setq output (f-swap-ext output "html")))
      (if (f-writable? output)
          (progn
            (make-variable-buffer-local 'markdown-export-with-name-file)
            (setq markdown-export-with-name-file output))
	(error (format "%s is not writable" output))))
    (princ (format "Exporting markdown to %s" markdown-export-with-name-file)))
  (let ((filename markdown-export-with-name-file)
	(bufname (make-temp-name "markdown-output-")))
    (markdown-standalone bufname)
    (with-current-buffer bufname
      (write-file filename nil)
      (kill-buffer))
    filename))

(defun markdown-preview-with-name ()
  "Export markdown and preview in browser."
  (interactive)
  (browse-url (markdown-export-with-name)))

(defun markdown-link-with-name ()
  "Export markdown and preview in eww."
  (interactive)
  (let ((current (buffer-name)))
    (eww-open-file (markdown-export-with-name))))

(use-package markdown-mode
  :after multistate
  :mode (("\\.md\\'" . markdown-mode)
	 ("\\.mdown\\'" . markdown-mode))
  :custom
  (markdown-command "pandoc -s --mathjax -f markdown -t html")
  (markdown-enable-html t)
  (markdown-enable-math t)
  :hook
  (markdown-mode . (lambda () (setq fill-column 80)))
  (markdown-mode . auto-fill-mode)
  :bind
  (:map markdown-mode-map
        ("C-M-i" . markdown-cycle))
  (:map multistate-command-state-map
	("e e" . markdown-export-with-name)
	("e p" . markdown-preview-with-name)
	("e l" . markdown-link-with-name)))

(provide 'init-markdown)

;;; init-markdown.el ends here
