;;; init-eldoc.el --- Initialize ElDoc -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize ElDoc

;;; Code:

(use-package eldoc
  :bind
  ("C-c h" . (lambda () (interactive)
	       (when eldoc-documentation-function
		 (let ((eldoc-echo-area-use-multiline-p t)
		       (msg))
		   (setq msg (funcall eldoc-documentation-function))
		   (if msg
		       (with-help-window (help-buffer)
			 (princ msg))
		     (message "Documentation not found")))))))

(provide 'init-eldoc)

;;; init-eldoc.el ends here
