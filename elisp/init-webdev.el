;;; init-webdev.el --- Initialize web -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; This initializes web-mode js2-mode typescript-mode emmet instant-rename-tag instant-rename-tag

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package web-mode
  :custom-face
  (css-selector ((t (:inherit default :foreground "#66CCFF"))))
  (font-lock-comment-face ((t (:foreground "#828282"))))
  :mode
  ("\\.phtml\\'" "\\.tpl\\.php\\'" "\\.[agj]sp\\'" "\\.as[cp]x\\'"
   "\\.erb\\'" "\\.mustache\\'" "\\.djhtml\\'" "\\.[t]?html?\\'")
  :init
  (when *html-lsp*
    (add-hook 'web-mode-hook #'lsp-maybe)))

(use-package js2-mode
  :mode ("\\.js\\'" . js2-mode)
  :interpreter "node"
  :init
  (when *npm*
    (add-hook 'js-mode-hook #'lsp-maybe))
  :config
  (setq js2-highlight-level 3)
  :custom
  (js-switch-indent-offset 8)
  :bind
  (:map js2-mode-map
        ("C-x x b" . (lambda () (interactive)
                       (save-current-buffer
                         (if (not (js-comint-get-buffer))
                             (js-comint-start-or-switch-to-repl)))
                       (js-send-buffer)))
        ("<C-return>" . (lambda () (interactive)
                          (save-current-buffer
                            (if (not (js-comint-get-buffer))
                                (js-comint-start-or-switch-to-repl)))
                          (if (use-region-p)
                              (progn
                                (js-send-region)
                                (setq mark-active nil))
                            (progn
                              (end-of-line)
                              (js-send-last-sexp)))))))

(use-package js-comint
  :if *node*
  :custom
  (js-comint-program-command *node*))

(use-package typescript-mode
  :mode "\\.ts\\'"
  :init
  (when *npm*
    (add-hook 'js-mode-hook #'lsp-maybe)))

(use-package json-mode
  :mode ("\\.json\\'" . json-mode)
  :init
  (when *json-lsp*
    (add-hook 'json-mode-hook #'lsp-maybe))
  :hook
  (json-mode . whitespace-mode)
  (json-mode . flymake-mode))

(use-package flymake-json)

(use-package yaml-mode
  :init
  (when *yaml-lsp*
    (add-hook 'yaml-mode-hook #'lsp-maybe))
  :hook
  (yaml-mode . highlight-indent-guides-mode)
  (yaml-mode . whitespace-mode)
  (yaml-mode . flymake-mode))

(use-package flymake-yaml)

(provide 'init-webdev)

;;; init-webdev.el ends here
