;;; init-icons.el --- Initialize icons -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize icons

;;; Code:

(eval-when-compile
  (require 'init-const)
  (require 'f))

(use-package all-the-icons
  :if *sys/gui*
  :config
  (let ((lock-file (f-join (f-full user-emacs-directory)
                           ".all-the-icons-fonts-installed")))
    (unless (f-file? lock-file)
      (all-the-icons-install-fonts t)
      (f-touch lock-file))))

(use-package all-the-icons-dired
  :after all-the-icons
  :if *sys/gui*
  :diminish
  :custom-face
  (all-the-icons-dired-dir-face ((t `(:foreground ,(face-background 'default)))))
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package nerd-icons
  :disabled
  :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  (nerd-icons-font-family "Symbols Nerd Font Mono"))

(provide 'init-icons)

;;; init-icons.el ends here
