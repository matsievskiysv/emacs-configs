;;; init-func.el --- Miscellaneous functions -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Miscellaneous functions

;;; Code:

(eval-when-compile
  (require 'init-global-config))

;; (defun decide-line-range (file begin end)
;;   "Visit FILE and decide which lines to include.
;; BEGIN and END are regexps which define the line range to use."
;;   (let (l r)
;;     (save-match-data
;;       (with-temp-buffer
;;         (insert-file-contents file)
;;         (goto-char (point-min))
;;         (if (null begin)
;;             (setq l "")
;;           (search-forward-regexp begin)
;;           (setq l (line-number-at-pos (match-beginning 0))))
;;         (if (null end)
;;             (setq r "")
;;           (search-forward-regexp end)
;;           (setq r (1+ (line-number-at-pos (match-end 0)))))
;;         (format "%s-%s" (+ l 1) (- r 1)))))) ;; Exclude wrapper
;; -OrgIncludeAuto

;; BetterMiniBuffer
;; (defun abort-minibuffer-using-mouse ()
;;   "Abort the minibuffer when using the mouse."
;;   (when (and (>= (recursion-depth) 1) (active-minibuffer-window))
;;     (abort-recursive-edit)))

;; (add-hook 'mouse-leave-buffer-hook 'abort-minibuffer-using-mouse)

;; keep the point out of the minibuffer
(setq-default minibuffer-prompt-properties '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt))
;; -BetterMiniBuffer

;; DisplayLineOverlay
(defun display-line-overlay+ (pos str &optional face)
  "Display line at POS as STR with FACE.

FACE defaults to inheriting from default and highlight."
  (let ((ol (save-excursion
              (goto-char pos)
              (make-overlay (line-beginning-position)
                            (line-end-position)))))
    (overlay-put ol 'display str)
    (overlay-put ol 'face
                 (or face '(:background null :inherit highlight)))
    ol))
;; -DisplayLineOverlay

;; ReadLines
(defun read-lines (file-path)
  "Return a list of lines of a file at FILE-PATH."
  (with-temp-buffer (insert-file-contents file-path)
                    (split-string (buffer-string) "\n" t)))
;; -ReadLines

;; WhereAmI
(defun where-am-i ()
  "An interactive function showing function `buffer-file-name' or `buffer-name'."
  (interactive)
  (let ((name (if (buffer-file-name) (buffer-file-name) (buffer-name))))
    (kill-new name)
    (message name)))
;; -WhereAmI

;; Comment set column arg
(defun comment-set-column-arg (num)
  "Interactive version of `comment-set-column'."
  (interactive "nNew comment column: ")
  (setq comment-column num))
;; -Comment set column arg

(defun kill-current-lines ()
  "Kill current lines."
  (interactive)
  (save-excursion
    (let*
        ((start (if (use-region-p) (region-beginning) (point)))
         (end (if (use-region-p) (region-end) (point)))
         (start (progn
                  (goto-char start)
                  (line-beginning-position)))
         (end (progn
                (goto-char end)
                (line-end-position))))
      (kill-region start end)
      (when (string-match-p "^$" (or (thing-at-point 'line) "miss"))
        (kill-line)))))

(defun duplicate-current-lines ()
  "Duplicate current lines."
  (interactive)
  (save-excursion
      (let*
          ((start (if (use-region-p) (region-beginning) (point)))
           (end (if (use-region-p) (region-end) (point)))
           (start (progn
                    (goto-char start)
                    (line-beginning-position)))
           (end (progn
                  (goto-char end)
                  (line-end-position)))
           (line (buffer-substring start end)))
        (goto-char end)
        (newline)
        (insert line))))

(defun which-active-modes ()
  "Which minor modes are enabled in the current buffer."
  (let ((active-modes))
    (mapc (lambda (mode) (condition-case nil
                        (if (and (symbolp mode) (symbol-value mode))
                            (add-to-list 'active-modes mode))
                      (error nil)))
          minor-mode-list)
    active-modes))

(defun rename-this-file (newfile)
  "Rename file opened in current buffer to."
  (interactive "*FEnter new file name: ")
  (let ((oldfile (buffer-file-name)))
    (write-file newfile t)
    (when (and oldfile (f-file? oldfile))
      (f-delete oldfile)))
)

(defun split-sentences ()
  "Move every sentence on new line and fill paragraph."
  (interactive)
  (save-excursion
    (save-restriction
      (let ((s (if (use-region-p)
                   (region-beginning)
                 (line-beginning-position)))
            (e (if (use-region-p)
                   (region-end)
                 (line-end-position))))
        (narrow-to-region s e)
	(let ((str (s-replace-all '((". " . ".\n")) (s-collapse-whitespace (buffer-substring-no-properties s e)))))
	  (delete-region s e)
	  (insert str))
	(goto-char (point-max))
	(while (> (point) (point-min))
	  (fill-region (line-beginning-position) (line-end-position))
          (forward-line -1))
        (fill-region (line-beginning-position) (line-end-position))))))

(defun copy-kill-at-point (thing act)
  "Copy or kill `THING' at point where `THING' is either `LINE' or `WORD',
 act is `COPY' or `KILL'."
       (save-excursion
         (let (l r)
           (when (s-equals? (s-downcase thing) "word")
             (search-backward-regexp "[[:space:]]" nil t)
             (forward-char)
             (setq l (point))
             (search-forward-regexp "[[:space:]]" nil t)
             (backward-char)
             (setq r (point)))
           (when (s-equals? (s-downcase thing) "line")
             (setq l (line-beginning-position))
             (setq r (line-end-position)))
           (unless (eq l r)
             (when (s-equals? (s-downcase act) "copy")
               (kill-ring-save l r))
             (when (s-equals? (s-downcase act) "kill")
               (kill-region l r)
               (when (string-match-p "^$" (or (thing-at-point 'line) "miss"))
                 (kill-line)))))))

(defun temp-buffer ()
  "Open new temporary buffer."
  (interactive)
  (switch-to-buffer (make-temp-name "scratch-")))

(defun chmod-this-file ()
  "Change this file mode."
  (interactive)
  (let ((file (buffer-file-name)))
    (if (and file (f-file? file))
        (set-file-modes file (read-file-modes nil file))
      (error "No file present"))))

(defmacro measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (message "%.06f" (float-time (time-since time)))))

(defun force-kill-buffer (buffer)
  "Force kill BUFFER."
  (interactive "b")
  (with-current-buffer buffer
    (let (kill-buffer-hook kill-buffer-query-functions)
      (kill-buffer))))

(defun save-all-buffers ()
  "Instead of `save-buffer', save all opened buffers by calling `save-some-buffers' with ARG t."
  (interactive)
  (save-some-buffers t))

(defun ansi-color-apply-on-region-int (beg end)
  "Apply ANSI colors to region"
  (interactive "r")
  (ansi-color-apply-on-region beg end))

(provide 'init-func)

;;; init-func.el ends here
