;;; init-ess.el --- Initialize ess -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize ess

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package ess
  :mode (("\\.R\\'" . ess-r-mode)
	 ("\\.jl\\'" . ess-julia-mode))
  :interpreter (("Rscript" . ess-r-mode)
		("julia" . ess-julia-mode))
  :custom
  (ess-tab-complete-in-script t)
  (inferior-R-args "--vanilla")
  :init
  (when (and *R* *bash*)
    (add-hook 'ess-r-mode-hook #'lsp-maybe))
  (when *julialsp*
    (add-hook 'ess-julia-mode-hook #'lsp-maybe)))

(use-package flycheck-julia)

;; julia> Pkg.add("PackageCompiler")
;; julia> using PackageCompiler
;; julia> compile_package("LanguageServer")
(use-package lsp-julia
  :if *julialsp*
  ;; :if *julia*
  :after lsp-mode
  :config
  (defun lsp-julia--rls-command ()
    "The command to lauch the Julia Language Server."
    '("JuliaLSP")))

(provide 'init-ess)

;;; init-ess.el ends here
