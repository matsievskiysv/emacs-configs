;;; init-adoc.el --- Initialize adoc-mode -*- lexical-binding: t -*-

;; Author: Matsievskiy S.V.
;; Maintainer: Matsievskiy S.V.
;; Version: 1.0
;; Package-Requires: (adoc-mode)
;; Homepage:
;; Keywords:


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize adoc-mode

;;; Code:

(use-package adoc-mode
  :after multistate
  :mode (("\\.adoc\\'" . adoc-mode))
  :hook
  (adoc-mode . (lambda () (setq fill-column 80)))
  (adoc-mode . auto-fill-mode))

(provide 'init-adoc)

;;; init-adoc.el ends here
