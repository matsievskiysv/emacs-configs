;;; init-const.el --- Initialize constants -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize constants

;;; Code:

;; {{{ UserInfo
(setq user-full-name "Matsievskiy S.V.")
(setq user-mail-address "matsievskiysv@gmail.com")
;; }}}


;; {{{ Consts
(defconst *sys/graphic*
  (display-graphic-p)
  "Are we running Emacs on X?")

(defconst *sys/server*
  (daemonp)
  "Are we running on a Emacs server?")

(defconst *sys/gui*
  (or *sys/graphic* *sys/server*)
  "Initialize graphics?")

(defconst *sys/root*
  (string-equal "root" (getenv "USER"))
  "Are you a ROOT user?")

(defconst *rg*
  (executable-find "rg")
  "Do we have ripgrep?")

(defconst *python3*
  (executable-find "python3")
  "Do we have python3?")

(defconst *tr*
  (executable-find "tr")
  "Do we have tr?")

(defconst *gcc*
  (executable-find "gcc")
  "Do we have gcc?")

(defconst *git*
  (executable-find "git")
  "Do we have git?")

(defconst *lualatex*
  (executable-find "lualatex")
  "Do we have lualatex?")

(defconst *clangd*
  (executable-find "clangd")
  "Do we have clangd?")

(defconst *ccls*
  (executable-find "ccls")
  "Do we have ccls?")

(defconst *bash*
  (executable-find "bash")
  "Do we have bash?")

(defconst *R*
  (executable-find "R")
  "Do we have R?")

(defconst *rust-lsp*
  (executable-find "rust-analyzer")
  "Do we have rust language server?")

(defconst *cargo*
  (executable-find "cargo")
  "Do we have rust cargo?")

(defconst *julia*
  (executable-find "julia")
  "Do we have julia?")

(defconst *julialsp*
  (executable-find "JuliaLSP")
  "Do we have julia lsp?")

(defconst *pyls*
  (executable-find "pylsp")
  "Do we have python lsp?")

(defconst *digestif*
  (executable-find "digestif")
  "Do we have LaTeX lsp?")

(defconst *json-lsp*
  (executable-find "vscode-json-languageserver")
  "Do we have json lsp?")

(defconst *shell-lsp*
  (executable-find "bash-language-server")
  "Do we have shell lsp?")

(defconst *html-lsp*
  (executable-find "html-languageserver")
  "Do we have html lsp?")

(defconst *yaml-lsp*
  (executable-find "yaml-language-server")
  "Do we have yaml lsp?")

(defconst *js-lsp*
  (executable-find "typescript-language-server")
  "Do we have js lsp?")

(defconst *npm*
  (executable-find "npm")
  "Do we have npm?")

(defconst *node*
  (executable-find "node")
  "Do we have node?")

(defconst *lua-lsp*
  (executable-find "EmmyLua")
  "Do we have Lua lsp?")

(defconst *cmake-lsp*
  (executable-find "cmake-language-server")
  "Do we have CMake lsp?")

(defconst *math-preview*
  (executable-find "math-preview")
  "Do we have math-preview?")

(defconst *kotlin-lsp*
  (executable-find "kotlin-language-server")
  "Do we have kotlin lsp?")

(defconst *vhdl-lsp*
  (executable-find "ghdl-ls")
  "Do we have VHDL language server?")

;; }}}

(provide 'init-const)

;;; init-const.el ends here
