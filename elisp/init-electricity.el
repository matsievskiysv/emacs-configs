;;; init-electricity.el --- -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialize parentheses

;;; Code:

(custom-set-variables '(electric-pair-mode nil)
		      '(electric-pair-delete-adjacent-pairs t)
		      '(electric-pair-skip-whitespace t)
		      '(electric-pair-pairs '((?\' . ?\')
					      (?\" . ?\")
					      (?\‘ . ?\’)
					      (?\“ . ?\”)
					      (?\` . ?\`)
					      (?\« . ?\»)))
		      '(electric-pair-text-pairs '((?\' . ?\')
						   (?\" . ?\")
						   (?\‘ . ?\’)
						   (?\“ . ?\”)
						   (?\` . ?\`)
						   (?\« . ?\»))))

(custom-set-variables '(electric-quote-mode nil)
		      '(electric-quote-string nil)
		      '(electric-quote-chars '(?\‘ ?\’ ?\“ ?\”)))

(provide 'init-electricity)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; init-electricity.el ends here
