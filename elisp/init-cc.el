;;; init-cc.el --- Initialize C -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize C

;;; Code:

(eval-when-compile
  (require 'init-const))


(use-package c-eldoc :defer t)
(custom-set-variables '(c-default-style "linux"))
(custom-set-variables '(c-basic-offset 8))
(c-set-offset 'comment-intro 0)
(c-set-offset 'innamespace 0)
(c-set-offset 'case-label '+)
(c-set-offset 'access-label 0)
(c-set-offset (quote cpp-macro) 0 nil)
(add-hook 'c-mode-hook (lambda () (c-toggle-comment-style -1)))
(add-hook 'c++-mode-hook (lambda () (c-toggle-comment-style -1)))

(if *ccls*
    (use-package ccls
      :config
      (setq ccls-executable *ccls*)))

(when (or *clangd* *ccls*)
  (add-hook 'c-mode-hook #'lsp-maybe)
  (add-hook 'c++-mode-hook #'lsp-maybe))

(use-package cmake-mode
  :config
  (when *cmake-lsp*
    (add-hook 'cmake-mode-hook #'lsp-maybe)))

(provide 'init-cc)

;;; init-cc.el ends here
