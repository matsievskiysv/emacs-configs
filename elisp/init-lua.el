;;; init-lua.el --- Initialize lua-mode -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize lua-mode

;;; Code:

(use-package lua-mode
  :mode ("\\.lua\\'" . lua-mode)
  :interpreter ("lua" . lua-mode)
  :bind
  (:map lua-mode-map
   ("C-c C-j" . lua-send-current-line)
   ("<C-return>" . (lambda () (interactive)
		     (if (use-region-p)
			 (progn
			   (lua-send-region
			    (region-beginning)
			    (region-end))
			   (setq mark-active nil))
		       (progn
			 (lua-send-current-line)
			 (forward-line 1)
			 (end-of-line))))))
  :hook
  (lua-mode . flymake-mode)
  (lua-mode . (lambda () (setq fill-column 80))))

(use-package flymake-lua)

;; (use-package lsp-lua-emmy
;;   :demand
;;   :ensure nil
;;   :load-path "~/github/lsp-lua-emmy"
;;   :hook (lua-mode . lsp)
;;   :config
;;   (setq lsp-lua-emmy-jar-path (expand-file-name "EmmyLua-LS-all.jar" user-emacs-directory)))

(provide 'init-lua)

;;; init-lua.el ends here
