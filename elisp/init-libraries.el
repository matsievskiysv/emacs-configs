;;; init-libraries.el --- Initialize libraries -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize libraries

;;; Code:

;; lists - https://github.com/magnars/dash.el
(use-package dash
  :config
  (with-eval-after-load 'dash (dash-enable-font-lock)))
(use-package dash-functional)

;; strings - https://github.com/magnars/s.el
(use-package s)

;; hash tables - https://github.com/Wilfred/ht.el
(use-package ht)

;; files - https://github.com/rejeep/f.el
(use-package f)

;; futures - https://github.com/jwiegley/emacs-async
(use-package async)

;; overlay manipulation - https://github.com/emacsorphanage/ov
(use-package ov)

;; namespaces - https://github.com/Malabarba/names
(use-package names)

;; browse URL
(require 'browse-url)

(provide 'init-libraries)

;;; init-libraries.el ends here
