;;; init-gnuplot.el --- Initialize gnuplot -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize gnuplot mode

;;; Code:

(use-package gnuplot
  :mode ("\\.gpi\\'" . gnuplot-mode)
  :interpreter ("gnuplot" . gnuplot-mode)
  :custom
  (gnuplot-tab-completion t)
  (gnuplot-context-sensitive-mode t)
  :bind
  (:map gnuplot-mode-map
        ("<C-return>" . (lambda () (interactive)
                          (gnuplot-send-line-to-gnuplot)
                          (forward-line 1)
                          (end-of-line))))
  :hook (gnuplot-mode . (lambda ()
			  (run-hooks `prog-mode-hook))))

(provide 'init-gnuplot)

;;; init-gnuplot.el ends here
