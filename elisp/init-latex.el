;;; init-latex.el --- Initialize latex -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize latex

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package auctex
  :mode (("\\.tex\\'" . LaTeX-mode)
	 ("\\.tikz\\'" . LaTeX-mode)
	 ("\\.bib\\'" . bibtex-mode))
  :custom
  (lsp-tex-server 'digestif)
  (TeX-parse-self t)
  (TeX-master 'dwim)
  (bibtex-comment-start "% ")
  (bibtex-dialect 'biblatex)
  (TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
  (LaTeX-indent-environment-list '(("verbatim" current-indentation)
                                   ("verbatim*" current-indentation)
                                   ("filecontents" current-indentation)
                                   ("filecontents*" current-indentation)
                                   ("tabular" LaTeX-indent-tabular)
                                   ("tabular*" LaTeX-indent-tabular)
                                   ("align")
                                   ("align*")
                                   ("array" LaTeX-indent-tabular)
                                   ("eqnarray" LaTeX-indent-tabular)
                                   ("eqnarray*" LaTeX-indent-tabular)
                                   ("comment" current-indentation)
                                   ("displaymath")
                                   ("equation")
                                   ("equation*")
                                   ("picture")
                                   ("tabbing")))
  (LaTeX-indent-level 8)
  (LaTeX-item-indent 0)
  (TeX-brace-indent-level 8)
  :init
  (when *digestif*
    (add-hook 'LaTeX-mode-hook #'lsp-maybe))
  ;; (TeX-auto-save t)
  (put 'LaTeX-narrow-to-environment 'disabled nil)
  :hook
  (LaTeX-mode . (lambda ()
                  (turn-on-reftex)
                  (reftex-isearch-minor-mode)
                  (setq reftex-plug-into-AUCTeX t
                        TeX-PDF-mode t
                        TeX-source-correlate-method 'synctex
                        TeX-source-correlate-start-server t
                        ispell-extra-args '("-t")
                        fill-column 100)))
  (bibtex-mode . (lambda ()
                   (run-hooks 'text-mode-hook)
                   (flycheck-mode -1)
                   (setq indent-line-function 'bibtex-fill-entry)))
  (LaTeX-mode . visual-line-mode)
  (LaTeX-mode . auto-fill-mode)
  (LaTeX-mode . hungry-delete-mode)
  (bibtex-mode . visual-line-mode)
  (bibtex-mode . auto-fill-mode))

(use-package auctex-lua
  :defer t)

(provide 'init-latex)

;;; init-latex.el ends here
