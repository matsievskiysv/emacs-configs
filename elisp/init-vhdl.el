;;; init-vhdl.el --- Initialize vhdl -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize vhdl

;;; Code:

(require 'init-const)

(use-package vhdl-mode
  :defer t
  :mode (("\\.vhdl\\'" . vhdl-mode)
	 ("\\.vhd\\'" . vhdl-mode))
  :config
  (setq lsp-vhdl-server 'ghdl-ls
        lsp-vhdl-server-path *vhdl-lsp*
        lsp-vhdl--params nil)
  (require 'lsp-vhdl)
  :hook (vhdl-mode . lsp-maybe)
)

(provide 'init-vhdl)

;;; init-vhdl.el ends here
