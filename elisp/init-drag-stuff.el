;;; init-drag-stuff.el --- Initialize drag-stuff -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize drag-stuff

;;; Code:

(use-package drag-stuff
  :diminish
  :after multistate
  :defer t
  :init
  (drag-stuff-global-mode 1)
  :bind
  (:map multistate-edit-state-map
	("<C-up>" . drag-stuff-up)
	("<C-down>" . drag-stuff-down)
	("<C-left>" . drag-stuff-left)
	("<C-right>" . drag-stuff-right))
  (:map multistate-move-state-map
	("d <up>" . drag-stuff-up)
	("d <down>" . drag-stuff-down)
	("d <left>" . drag-stuff-left)
	("d <right>" . drag-stuff-right)))

(provide 'init-drag-stuff)

;;; init-drag-stuff.el ends here
