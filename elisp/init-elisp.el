;;; init-elisp.el --- Initialize elisp -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize elisp

;;; Code:

(defun show-lisp-help-at-point ()
  "Display function or variable at point in *Help* buffer."
  (interactive)
  (let ((rgr-symbol (symbol-at-point))) ; symbol-at-point http://www.emacswiki.org/cgi-bin/wiki/thingatpt%2B.el
    (if (and rgr-symbol
	       (or (fboundp rgr-symbol)
		   (boundp rgr-symbol)))
      (if (fboundp rgr-symbol)
          (describe-function rgr-symbol)
	(when (boundp rgr-symbol)
          (describe-variable rgr-symbol)))
      (message "Nothing found"))))

(setq inferior-lisp-program "clisp")

(bind-key "C-c h" #'show-lisp-help-at-point lisp-interaction-mode-map)
(bind-key "C-c h" #'show-lisp-help-at-point lisp-mode-map)
(bind-key "C-c h" #'show-lisp-help-at-point emacs-lisp-mode-map)
(bind-key "<C-return>" (lambda () (interactive)
			 (end-of-line)
			 (eval-print-last-sexp))
	  lisp-interaction-mode-map)

(use-package package-lint :defer t)

(provide 'init-elisp)

;;; init-elisp.el ends here
