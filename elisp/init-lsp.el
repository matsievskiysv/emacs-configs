;;; init-lsp.el --- Initialize server protocol -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize server protocol

;;; Code:

(use-package lsp-mode
  :after multistate
  :defer t
  :commands lsp
  :custom
  (lsp-auto-guess-root t)
  (lsp-lens-enable nil)
  (lsp-file-watch-threshold 2000)
  (lsp-clients-r-server-command '("bash" "R" "--vanilla" "--slave" "-e" "languageserver::run()"))
  (read-process-output-max (* 1024 1024))
  (lsp-enable-suggest-server-download nil)
  :config
  (bind-key "z" lsp-command-map multistate-move-state-map)
  (which-key-add-key-based-replacements
    "z =" "formatting"
    "z a" "code actions"
    "z F" "folders"
    "z G" "peek"
    "z T" "features"
    "z g" "goto"
    "z h" "help"
    "z r" "refactor"
    "z w" "sessions"
    "z t" "toggle")
  (setq lsp-maybe-flag t)
  (let ((config-file (f-join user-emacs-directory ".lsp-maybe")))
    (unless (f-file? config-file)
      (with-temp-buffer
        (erase-buffer)
        (insert (symbol-name lsp-maybe-flag))
        (write-file config-file)))
    (with-temp-buffer
      (erase-buffer)
      (insert-file-contents config-file)
      (setq lsp-maybe-flag (and (intern (s-trim (buffer-string)))))))
  (defun lsp-maybe ()
    "Maybe start LSP"
    (interactive)
    (when lsp-maybe-flag
      (lsp)))
  (defun lsp-maybe-toggle ()
    "Toggle maybe start LSP flag"
    (interactive)
    (setq lsp-maybe-flag
          (not lsp-maybe-flag))
    (let ((config-file (f-join user-emacs-directory ".lsp-maybe")))
      (with-temp-buffer
        (erase-buffer)
        (insert (symbol-name lsp-maybe-flag))
        (write-file config-file)))
    (message
     (if lsp-maybe-flag
         "LSP hook enabled"
       "LSP hook disabled"))))

(use-package lsp-ui
  :after lsp-mode
  :diminish
  :commands lsp-ui-mode
  :custom
  (lsp-ui-doc-enable nil)
  (lsp-ui-doc-position 'bottom)
  (lsp-ui-peek-always-show t)
  :bind
  (:map lsp-command-map
        ("T i" . lsp-ui-imenu))
  :hook
  (lsp-after-initialize . lsp-ui-mode))

(use-package lsp-treemacs
  :after (lsp-mode treemacs))

(use-package dap-mode
  :diminish
  :after multistate
  :bind
  (:map multistate-command-state-map
        ("d d" . dap-debug)
	("d c" . dap-continue)
	("d n" . dap-next)
	("d i" . dap-step-in)
	("d o" . dap-step-out)
	("d SPC" . dap-breakpoint-toggle)))

;; separate history per window
(setq xref-history-storage 'xref-window-local-history)

(provide 'init-lsp)

;;; nit-lsp.el ends here
