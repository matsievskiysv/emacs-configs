;;; init-mode-line.el --- Initialize mode-line -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize mode-line

;;; Code:

(use-package simple-modeline
  :init
  (simple-modeline-mode t)
  :custom
  (simple-modeline-segments '((simple-modeline-segment-modified
                               simple-modeline-segment-input-method
                               simple-modeline-segment-buffer-name
                               simple-modeline-segment-position)
                              (simple-modeline-segment-minor-modes
                               simple-modeline-segment-vc
                               simple-modeline-segment-misc-info
                               simple-modeline-segment-process
                               simple-modeline-segment-encoding
                               simple-modeline-segment-eol
                               simple-modeline-segment-major-mode))))

(provide 'init-mode-line)

;;; init-mode-line.el ends here
