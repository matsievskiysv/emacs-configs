;;; init-undo-tree.el --- Initialize undo-tree -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize undo-tree

;;; Code:

(use-package undo-tree
  :diminish undo-tree-mode
  :init (global-undo-tree-mode)
  :custom
  (undo-tree-visualizer-diff nil)
  (undo-tree-visualizer-timestamps t)
  (undo-tree-enable-undo-in-region nil)
  (undo-tree-auto-save-history nil)
  :bind
  ("M-/" . 'undo-tree-visualize)
  (:map undo-tree-visualizer-mode-map
        ("RET" . undo-tree-visualizer-quit)
        ("RET" . undo-tree-visualizer-quit)))

(provide 'init-undo-tree)

;;; init-undo-tree.el ends here
