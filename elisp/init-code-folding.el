;;; init-code-folding.el --- Initialize code-folding -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize vimish-fold

;;; Code:

;; vimish-fold - fold code
(use-package vimish-fold
  :after multistate
  :ensure t
  :custom
  (vimish-fold-find-marks-on-open t)
  (vimish-fold-header-width nil)
  :config
  (unbind-key "C-`" vimish-fold-unfolded-keymap)
  :init
  (vimish-fold-global-mode t)
  :bind
  (:map multistate-move-state-map
	("f f" . vimish-fold)
	("f d" . vimish-fold-unfold)
	("f a d" . vimish-fold-unfold-all)
	("f o d" . vimish-fold-unfold-all-others)
	("f r" . vimish-fold-refold)
	("f a r" . vimish-fold-refold-all)
	("f o r" . vimish-fold-refold-all-others)
	("f <deletechar>" . vimish-fold-delete)
	("f a <deletechar>" . vimish-fold-delete-all)
	("f o <deletechar>" . vimish-fold-delete-all-others)
	("f SPC" . vimish-fold-toggle)
	("f a SPC" . vimish-fold-toggle-all)
	("f o SPC" . vimish-fold-toggle-all-others)
	("f <right>" . vimish-fold-next-fold)
	("f <down>" . vimish-fold-next-fold)
	("f n" . vimish-fold-next-fold)
	("f <up>" . vimish-fold-previous-fold)
	("f <left>" . vimish-fold-previous-fold)
	("f p" . vimish-fold-previous-fold)
	("f m" . vimish-fold-from-marks)
	("f w" . vimish-fold-narrow-to-fold)))

(provide 'init-code-folding)

;;; init-code-folding ends here
