;;; init-flyspell.el --- Initialize flyspell -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize flyspell

;;; Code:

(eval-when-compile
  (require 'dash)
  (require 'f)
  (require 's))

(use-package popup)

(use-package flyspell
  :diminish
  :config
  (setq ispell-program-name "hunspell")
  (setq ispell-personal-dictionary (f-join user-emacs-directory "assets" "dicts" "personal"))
  (setq hunspell-local-dicts
        (s-join "," (->> (f-join user-emacs-directory "assets" "dicts")
                         (f-glob "*.dic")
                         (-map 'f-no-ext)
                         (-sort 'string>))))
  (setq hunspell-user-dicts (s-join "," `("en_US" ,hunspell-local-dicts)))
  (setq-default ispell-local-dictionary hunspell-user-dicts)
  (setq ispell-hunspell-dictionary-alist
        `((,hunspell-user-dicts "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" ,hunspell-user-dicts) nil utf-8)))
  (setq flyspell-issue-welcome-flag nil)
  (setq-default ispell-list-command "list")
  (unbind-key "C-;" flyspell-mode-map)
  (unbind-key "C-M-i" flyspell-mode-map)
  (defun flyspell-emacs-popup-textual (event poss word)
    "A textual flyspell popup menu."
    (require 'popup)
    (let* ((corrects (if flyspell-sort-corrections
                         (sort (car (cdr (cdr poss))) 'string<)
                       (car (cdr (cdr poss)))))
           (cor-menu (if (consp corrects)
                         (mapcar (lambda (correct)
                                   (list correct correct))
                                 corrects)
                       '()))
           (affix (car (cdr (cdr (cdr poss)))))
           show-affix-info
           (base-menu  (let ((save (if (and (consp affix) show-affix-info)
                                       (list
                                        (list (concat "Save affix: " (car affix))
                                              'save)
                                        '("Accept (session)" session)
                                        '("Accept (buffer)" buffer))
                                     '(("Save word" save)
                                       ("Accept (session)" session)
                                       ("Accept (buffer)" buffer)))))
                         (if (consp cor-menu)
                             (append cor-menu (cons "" save))
                           save)))
           (menu (mapcar
                  (lambda (arg) (if (consp arg) (car arg) arg))
                  base-menu)))
      (cadr (assoc (popup-menu* menu :scroll-bar t) base-menu))))
  (defun flyspell-emacs-popup-choose (org-fun event poss word)
    (if (display-graphic-p)
        (funcall org-fun event poss word)
      (flyspell-emacs-popup-textual event poss word)))
  (advice-add 'flyspell-emacs-popup :around #'flyspell-emacs-popup-choose)
  :hook
  ((prog-mode . flyspell-prog-mode)
   (text-mode . flyspell-mode)))

(provide 'init-flyspell)

;;; flyspell.el ends here
