;;; init-avy.el --- Initialize avy -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize avy

;;; Code:

(use-package avy
  :after multistate
  :custom
  (avy-style 'at)
  :bind
  ("C-x x SPC" . avy-goto-word-0)
  (:map multistate-move-state-map
	("SPC" . avy-goto-word-0)
	("M-SPC" . avy-goto-char-timer)))

(provide 'init-avy)

;;; init-avy.el ends here
