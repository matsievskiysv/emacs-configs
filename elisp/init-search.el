;;; init-search.el --- Initialize avy, counsel and swiper -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize avy, counsel and swiper

;;; Code:

(use-package ivy
  :diminish
  :init
  (use-package amx :defer t)
  (use-package counsel :diminish :config (counsel-mode 1))
  (use-package swiper :defer t)
  (ivy-mode 1)
  :bind
  ("C-s" . swiper-isearch)
  ("C-r" . swiper-isearch-backward)
  ("C-x s" . counsel-rg)
  ("C-x b" . counsel-ibuffer)
  ("C-x C-r" . counsel-recentf)
  ;; ("C-x 8 RET" . counsel-unicode-char)
  (:map ivy-minibuffer-map
        ("C-r" . ivy-previous-line-or-history)
        ("M-RET" . ivy-immediate-done)
        ("<tab>" . ivy-partial)
        ("RET" . ivy-alt-done)
        ("C-j" . ivy-done))
  :custom
  (ivy-use-virtual-buffers t)
  (ivy-height 10)
  (ivy-on-del-error-function 'ignore)
  (ivy-magic-slash-non-match-action 'ivy-magic-slash-non-match-cd-selected)
  (ivy-count-format "【%d/%d】"))

;; disable line truncation in minibuffer
(advice-add 'counsel-find-file :around
            (lambda (oldfunc &rest args)
              (let ((ivy-truncate-lines nil))
                (apply oldfunc args))))


(use-package frog-jump-buffer
  :disabled
  :ensure t
  :bind
  ("C-x M-b" . frog-jump-buffer)
  :custom-face
  (frog-menu-posframe-background-face ((t nil))))


(use-package insert-char-preview
  :bind
  ("C-x 8 RET" . insert-char-preview))

(provide 'init-search)

;;; init-search.el ends here
