;;; init-global-config.el --- Global configuration -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Global configuration

;;; Code:

(eval-when-compile
  (require 'init-const))

;; Move Custom-Set-Variables to Different File
(setq custom-file (concat user-emacs-directory "custom-set-variables.el"))
(load custom-file 'noerror)

;; {{{ sudo edit
(use-package sudo-edit
  :commands (sudo-edit))
;; }}}

;; {{{ UTF8Coding
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(when *sys/gui*
  (custom-set-variables '(x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))))
;; }}}

;; {{{ History
(use-package recentf
  :ensure nil
  :hook (after-init . recentf-mode)
  :custom
  (recentf-auto-cleanup "05:00am")
  (recentf-max-saved-items 200)
  (recentf-exclude '((expand-file-name package-user-dir)
                     ".cache"
                     ".cask"
                     ".elfeed"
                     "bookmarks"
                     "cache"
                     "ido.*"
                     "persp-confs"
                     "recentf"
                     "undo-tree-hist"
                     "url"
                     "COMMIT_EDITMSG\\'")))


(custom-set-variables '(history-length 500) ;; Set history-length longer
		      '(save-place-mode 1)) ;; When buffer is closed, saves the cursor location

;; }}}

(custom-set-variables '(default-input-method 'russian-computer) ;; Input method
		      '(comment-column 82) ;; Comment column
		      '(echo-keystrokes 0.1) ;; Show Keystrokes in Progress Instantly
		      '(electric-indent-mode 1) ;; Enable electric indent
		      '(fill-column 80) ;; Autofill column
                      '(truncate-lines t) ;; Truncate long lines
		      '(require-final-newline t) ;; Require new line
		      '(delete-selection-mode 1)) ;; Replace selection on insert

;; Desktop save mode
(custom-set-variables '(desktop-save-mode nil)
		      '(desktop-load-locked-desktop nil)
                      '(desktop-restore-eager 3)
                      '(desktop-restore-frames nil)
                      '(desktop-save t)
                      '(desktop-path '("~/.emacs.d/")))

;; Line indicator
(if (fboundp 'global-display-fill-column-indicator-mode)
    (global-display-fill-column-indicator-mode))

;; Better Compilation
(custom-set-variables '(compilation-always-kill t) ;; kill compilation process before starting another
		      '(compilation-ask-about-save nil) ;; save all buffers on `compile'
		      '(compilation-scroll-output t))

;; So Long mitigates slowness due to extremely long lines.
(when (fboundp 'global-so-long-mode)
  (global-so-long-mode))

;; Disable bidirectional paragraphs
(custom-set-variables
 '(bidi-paragraph-direction 'left-to-right))
(if (version<= "27.1" emacs-version)
    (setq bidi-inhibit-bpa t))

;; Default .args, .in, .out files to text-mode
(add-to-list 'auto-mode-alist '("\\.in\\'" . text-mode))
(add-to-list 'auto-mode-alist '("\\.out\\'" . text-mode))
(add-to-list 'auto-mode-alist '("\\.args\\'" . text-mode))

(use-package hungry-delete
  :diminish
  :config
  (setq hungry-delete-chars-to-skip " \t\r\f\v")
  :hook
  (prog-mode . hungry-delete-mode))

;; yes-or-no alias
(fset 'yes-or-no-p 'y-or-n-p)

;; Enable functions
(put 'scroll-left 'disabled nil)
(put 'scroll-right 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)

(use-package list-unicode-display
  :commands list-unicode-display)

(use-package diffview)

(provide 'init-global-config)

;;; init-global-config.el ends here
