;;; init-indent-or-complete.el --- Indent or complete at point -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Indent or complete at point
;;; Code:

(require 'dash)

(defun check-expansion ()
  "Check if completing company."
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "->") t nil)))))

(defun in-yas-field ()
  "Check if in yas field"
  (-any? #'identity (--map
                     (overlay-get it 'yas--field)
                     (overlays-in (- (point) 1) (point)))))

(defun tab-indent-or-complete ()
  "Indent or complete code."
  (interactive)
  (cond ((minibufferp) (completion-at-point))
        ((string= major-mode "magit-status-mode")
         (magit-section-toggle (magit-current-section)))
        ((use-region-p) (indent-region (region-beginning) (region-end)))
        ((and yas/minor-mode (yas-expand)))
        ((in-yas-field) (yas-next-field))
        ((check-expansion) (company-complete))
        (t (indent-for-tab-command))))

(bind-key "<tab>" 'tab-indent-or-complete)

(provide 'init-indent-or-complete)

;;; init-indent-or-complete.el ends here
