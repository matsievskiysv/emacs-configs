;;; init-visual-replace.el --- Initialize visual regex steroids -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize visual regexp

;;; Code:

(use-package multiple-cursors
  :after multistate
  :custom
  (mc/always-run-for-all nil)
  (mc/list-file (f-join user-emacs-directory "mc-lists.el"))
  :bind
  ("C-S-<mouse-1>" . mc/add-cursor-on-click)
  (:map multistate-edit-state-map
	("m l" . mc/edit-lines)
	("m a" . mc/edit-beginnings-of-lines)
	("m e" . mc/edit-ends-of-lines)
	("m SPC" . mc/mark-all-dwim)))

(use-package visual-regexp-steroids
  :demand t
  :after multistate
  :custom
  (vr/command-python (format "python3 %s"
                             (car (f-glob "visual-regexp-steroids*/regexp.py"
                                          (f-join (f-full user-emacs-directory)
                                                  "elpa")))))
  (vr/engine 'python)
  :bind
  (:map multistate-edit-state-map
	("r r" . vr/replace)
	("r q" . vr/query-replace)
	("r s" . vr/select-query-replace)
	("r m" . vr/mc-mark)))

(provide 'init-visual-replace)

;;; init-visual-replace.el ends here
