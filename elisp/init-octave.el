;;; init-octave.el --- Initialize octave -*- lexical-binding: t -*-


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize octave

;;; Code:

(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))
(with-eval-after-load "octave"
  (bind-key "<C-return>" (lambda () (interactive)
			   (if (not (region-active-p))
			       (octave-send-line)
			     (octave-send-region (region-beginning) (region-end))
			     (deactivate-mark)))
	    octave-mode-map))


(provide 'init-octave)

;;; init-octave.el ends here
