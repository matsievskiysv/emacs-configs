;;; init-ui-config.el --- Initialize UI -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize UI

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package page-break-lines
  :diminish
  :hook
  ((emacs-lisp-mode . page-break-lines-mode)
   (lisp-interaction-mode . page-break-lines-mode)
   (compilation-mode . page-break-lines-mode)))

(setq-default frame-title-format '( "%b - " user-login-name "@" system-name))

(custom-set-variables '(show-trailing-whitespace t) ;; Show trailing whitespaces
		      '(visible-bell t) ;; Visible bell
		      '(use-dialog-box nil) ;; Do not use dialog box
		      '(size-indication-mode t) ;; Show buffer size
		      ;; '(display-time-mode 1)
		      ;; '(display-battery-mode 1)
		      '(blink-cursor-mode 1) ;; Blink cursor
		      '(inhibit-startup-screen t)
		      '(initial-major-mode 'text-mode)
		      '(initial-scratch-message "")
		      '(global-display-line-numbers-mode 1)
		      '(column-number-mode 1))

;; {{{ Visual line
(add-hook 'text-mode-hook 'visual-line-mode)
(diminish 'visual-line-mode)
;; }}}

;; Disable menu bar in X
(when *sys/gui*
  (menu-bar-mode -1))

(provide 'init-ui-config)

;;; init-ui-config.el ends here
