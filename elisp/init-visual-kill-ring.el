;;; init-visual-kill-ring.el --- Initialize visual kill ring -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize visual kill ring

;;; Code:

(use-package browse-kill-ring
  :custom
  (browse-kill-ring-display-duplicates nil)
  (browse-kill-ring-highlight-inserted t)
  (browse-kill-ring-highlight-current-entry t)
  :bind
  ("M-y" . browse-kill-ring)
  (:map browse-kill-ring-mode-map
        ("<C-up>" . browse-kill-ring-previous)
        ("<C-down>" . browse-kill-ring-forward)))

(provide 'init-visual-kill-ring)

;;; init-visual-kill-ring.el ends here
