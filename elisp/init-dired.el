;;; init-dired.el --- Initialize dired -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize dired

;;; Code:

(use-package dired
  :disabled
  :ensure nil
  :bind
  ("C-x C-d" . dired-jump)
  :custom
  ;; Always delete and copy recursively
  (dired-recursive-deletes 'always)
  (dired-recursive-copies 'always)
  ;; Auto refresh Dired, but be quiet about it
  (global-auto-revert-non-file-buffers t)
  (auto-revert-verbose nil)
  ;; Quickly copy/move file in Dired
  (dired-dwim-target t)
  ;; Move files to trash when deleting
  (delete-by-moving-to-trash t)
  ;; Load the newest version of a file
  (load-prefer-newer t)
  ;; Detect external file changes and auto refresh file
  (auto-revert-use-notify nil)
  (auto-revert-interval 3) ; Auto revert every 3 sec
  :config
  ;; Enable global auto-revert
  (global-auto-revert-mode t)
  ;; Reuse same dired buffer, to prevent numerous buffers while navigating in dired
  (put 'dired-find-alternate-file 'disabled nil))

(use-package disk-usage
  :commands (disk-usage))

(use-package super-save
  :diminish
  :disabled
  :custom
  (super-save-auto-save-when-idle t)
  (auto-save-default nil)
  (make-backup-files nil)
  :init
  (super-save-mode 1))

(use-package neotree
  :config
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  (defun neotree--close-window-on-file-open (&rest args)
    "Close neotree window when opening file."
    (interactive)
    (neotree-hide))
  (advice-add #'neo-open-file :after #'neotree--close-window-on-file-open)
  :custom
  (neo-smart-open t)
  :bind
  ("C-x C-d" . neotree-toggle))

(provide 'init-dired)

;;; init-dired.el ends here
