;;; init-project.el --- Initialize project packages -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Initialize project packages

;;; Code:

(eval-when-compile
  (require 'init-const))

(use-package treemacs
  ;; :init
  ;; (with-eval-after-load 'winum
  ;;   (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :ensure t
  :custom
  (treemacs-no-png-images nil)
  (treemacs-persist-file (expand-file-name ".cache/treemacs-persist" user-emacs-directory))
  (treemacs-show-cursor nil)
  (treemacs-show-hidden-files t)
  :config
  ;; The default width and height of the icons is 22 pixels. If you are
  ;; using a Hi-DPI display, uncomment this to double the icon size.
  ;;(treemacs-resize-icons 44)
  (defun treemacs--close-window-on-file-open (&rest args)
    "Close treemacs window when opening file."
    (interactive)
    (treemacs-select-window)
    (treemacs-quit))
  (advice-add #'treemacs-visit-node-no-split :after #'treemacs--close-window-on-file-open)
  (treemacs-follow-mode nil)
  (treemacs-filewatch-mode t)
  (treemacs-fringe-indicator-mode t)
  (treemacs-indent-guide-mode t)
  :bind
  ("M-0" . treemacs-select-window)
  (:map treemacs-mode-map
	("f" . treemacs-find-file)
	("<C-left>" . treemacs-goto-parent-node)
	("<C-right>" . treemacs-TAB-action)))

(defun projectile-discover-my-projects ()
  "Discover projects in ~/Projects"
  (interactive)
  (when (f-dir? "~/Projects/")
    (message "Looking for projects...")
    (mapcar #'projectile-discover-projects-in-directory
            (f-directories "~/Projects/"))
    (message "Done")))

(use-package projectile
  :diminish
  :after multistate
  :custom
  (projectile-completion-system 'ivy)
  :config
  (bind-key "p" projectile-command-map multistate-command-state-map)
  (bind-key "SPC A" #'projectile-add-known-project projectile-command-map)
  (bind-key "SPC d" #'projectile-discover-my-projects projectile-command-map)
  (projectile-mode 1)
  (add-to-list 'projectile-globally-ignored-directories "node_modules")
  (projectile-register-project-type 'julia '("Project.toml" "src"))
  (add-to-list 'projectile-project-root-files "Project.toml"))

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t
  :config
  (bind-key "SPC p" #'treemacs-projectile projectile-command-map)
  (bind-key "SPC a" #'treemacs-add-project-to-workspace projectile-command-map))

(use-package treemacs-magit
  :after treemacs magit
  :ensure t)

(use-package treemacs-nerd-icons
  :disabled
  :after nerd-icons
  :config
  (add-hook 'server-after-make-frame-hook
            (lambda () (if (display-graphic-p)
                      (treemacs-load-theme "Default")
                    (treemacs-load-theme "nerd-icons")))))

(provide 'init-project)

;;; init-project.el ends here
